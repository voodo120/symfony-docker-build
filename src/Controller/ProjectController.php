<?php

namespace App\Controller;

use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProjectController extends AbstractController
{
    #[Route('/index', name: 'index')]
    public function index(): Response
    {
        
        return $this->render('/project/index.html.twig');
    }

    #[Route('/data', name: 'data')]
    public function data(): Response
    {
        $data = $this->getDoctrine()->getRepository(Project::class)->findAll();
        $output = array();
        $i = 0;
        /*foreach($data as $d)
        {
            $output[$i] = [
                'title' => $d->getTitle(),
                'description' => $d->getDescription(),
            ];
            $i = $i + 1;
        }
        */       
        
        return $this->render('/project/data.html.twig',array(
            'output' => $data,
        ));
    }

    #[Route('/new', name: 'new')]
    public function new(Request $request): Response
    {
        $data = new Project();

        $form = $this->createFormBuilder($data)
            ->add('title',TextType::class)
            ->add('description',TextType::class)
            ->add('save',SubmitType::class,array('label' => 'Add New Project'))
            ->getForm();

            $form->handleRequest($request);
            
            if ($form->isSubmitted() && $form->isValid()) { 
                $data = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();
                
                return $this->redirectToRoute('index');
            }

        return $this->render('project/new.html.twig',[
            'form' => $form->createView(),
        ]);
    }
    #[Route('/show/{id}', name: 'show')]
    public function show($id): Response
    {
        $data = $this->getDoctrine()->getRepository(Project::class)->find($id);

        return $this->render('project/show.html.twig',array(
            'data' => $data,
        ));
    }

    #[Route('delete/{id}', name: "delete")]
    public function delete($id):Response
    {

        $object = $this->getDoctrine()->getRepository(Project::class)->find($id);

        if($object)
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($object);
            $em->flush();
            return $this->render('project/index.html.twig');
        }else
        {
            return new Response("Error object not found");
        }



    }
}
